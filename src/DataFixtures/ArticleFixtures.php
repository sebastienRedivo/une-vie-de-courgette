<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Blog;
use App\Entity\Comment;
use App\Entity\Category;
use App\Entity\Picture;


class ArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');

        $user = new User();
        $user
            ->setAdress($faker->address)
            ->setFirstname($faker->firstName)
            ->setLastname($faker->lastName)
            ->setMail("testuser@test.fr")
            ->setPassword(password_hash("azerty", PASSWORD_DEFAULT))
            ->setUsername("testuser")
            ->setRoles(['ROLE_ADMIN']);

        $manager->persist($user);
        $manager->flush();

        // Add category's, articles & comments
        for ($i = 0; $i <= 2; $i++) {
            $category = new Category();
            $category
                ->setName($faker->jobTitle)
                ->setDescription($faker->sentence());

            $manager->persist($category);

            for ($j = 1; $j <= 10; $j++) {
                $article = new Blog();
                $content = '<p>' . join($faker->paragraphs(5), '<p></p>') . '</p>';

                $article
                    ->setTitle($faker->sentence())
                    ->setContent($content)
                    ->setUser($user)
                    // ->setImage($faker->imageUrl($width = 640, $height = 480))
                    ->setCreatedAt($faker->dateTimeBetween('-6 months'))
                    ->setCategory($category)
                    ->setModerate(1);

                $manager->persist($article);

                for ($b = 0; $b <= 2; $b++) {
                    $pictures = new Picture();

                    $pictures
                        ->setBlog($article)
                        ->setAlt($faker->country)
                        ->setName($faker->image('public/images/uploaded' ,$width = 600, $height = 150, 'food', false));
                    $manager->persist($pictures);
                }

                for ($k = 1; $k <= 10; $k++) {
                    $comment = new Comment();
                    $content = '<p>' . join($faker->paragraphs(2), '<p></p>') . '</p>';
                    $days = (new \DateTime())->diff($article->getCreatedAt())->days;

                    $comment
                        ->setPseudo($faker->name)
                        ->setContent($content)
                        ->setCreatedAt($faker->dateTimeBetween('_' . $days . ' days'))
                        ->setBlog($article)
                        ->setModerate(1);

                    $manager->persist($comment);
                }
            }
        }

        $manager->flush();
    }
}
