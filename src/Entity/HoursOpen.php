<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HoursOpenRepository")
 */
class HoursOpen
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="time")
     */
    private $hourStart;

    /**
     * @ORM\Column(type="time")
     */
    private $hourEnd;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $idDate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHourStart(): ?\DateTimeInterface
    {
        return $this->hourStart;
    }

    public function setHourStart(\DateTimeInterface $hourStart): self
    {
        $this->hourStart = $hourStart;

        return $this;
    }

    public function getHourEnd(): ?\DateTimeInterface
    {
        return $this->hourEnd;
    }

    public function setHourEnd(\DateTimeInterface $hourEnd): self
    {
        $this->hourEnd = $hourEnd;

        return $this;
    }

    public function getIdDate(): ?string
    {
        return $this->idDate;
    }

    public function setIdDate(?string $idDate): self
    {
        $this->idDate = $idDate;

        return $this;
    }
}
