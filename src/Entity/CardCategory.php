<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CardCategoryRepository")
 */
class CardCategory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CardElement", mappedBy="category")
     */
    private $cardElements;

    public function __construct()
    {
        $this->cardElements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|CardElement[]
     */
    public function getCardElements(): Collection
    {
        return $this->cardElements;
    }

    public function addCardElement(CardElement $cardElement): self
    {
        if (!$this->cardElements->contains($cardElement)) {
            $this->cardElements[] = $cardElement;
            $cardElement->setCategory($this);
        }

        return $this;
    }

    public function removeCardElement(CardElement $cardElement): self
    {
        if ($this->cardElements->contains($cardElement)) {
            $this->cardElements->removeElement($cardElement);
            // set the owning side to null (unless already changed)
            if ($cardElement->getCategory() === $this) {
                $cardElement->setCategory(null);
            }
        }

        return $this;
    }
}
