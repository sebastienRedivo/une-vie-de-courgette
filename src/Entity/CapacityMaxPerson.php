<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CapacityMaxPersonRepository")
 */
class CapacityMaxPerson
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $weekday;

    /**
     * @ORM\Column(type="integer")
     */
    private $capacityMax;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWeekday(): ?string
    {
        return $this->weekday;
    }

    public function setWeekday(string $weekday): self
    {
        $this->weekday = $weekday;

        return $this;
    }

    public function getCapacityMax(): ?string
    {
        return $this->capacityMax;
    }

    public function setCapacityMax(string $capacityMax): self
    {
        $this->capacityMax = $capacityMax;

        return $this;
    }
}
