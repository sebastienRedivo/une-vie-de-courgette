<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TypeFoodRepository")
 */
class TypeFood
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Foodtype", mappedBy="typeFood")
     */
    private $typeFood;

    public function __construct()
    {
        $this->typeFood = new ArrayCollection();
        $this->menus = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|Foodtype[]
     */
    public function getTypeFood(): Collection
    {
        return $this->typeFood;
    }

    public function addTypeFood(Foodtype $typeFood): self
    {
        if (!$this->typeFood->contains($typeFood)) {
            $this->typeFood[] = $typeFood;
            $typeFood->setTypeFood($this);
        }

        return $this;
    }

    public function removeTypeFood(Foodtype $typeFood): self
    {
        if ($this->typeFood->contains($typeFood)) {
            $this->typeFood->removeElement($typeFood);
            // set the owning side to null (unless already changed)
            if ($typeFood->getTypeFood() === $this) {
                $typeFood->setTypeFood(null);
            }
        }

        return $this;
    }


}
