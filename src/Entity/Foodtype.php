<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FoodtypeRepository")
 */
class Foodtype
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Menu", cascade={"persist"}, inversedBy="foodtypes")
     */
    private $compomenu;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeFood", cascade={"persist"}, inversedBy="typeFood")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeFood;

    public function __construct()
    {
        $this->compomenu = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getOrderinmenu(): ?string
    {
        return $this->orderinmenu;
    }

    public function setOrderinmenu(string $orderinmenu): self
    {
        $this->orderinmenu = $orderinmenu;

        return $this;
    }

    /**
     * @return Collection|Menu[]
     */
    public function getCompomenu(): Collection
    {
        return $this->compomenu;
    }

    public function addCompomenu(Menu $compomenu): self
    {
        if (!$this->compomenu->contains($compomenu)) {
            $this->compomenu[] = $compomenu;
        }

        return $this;
    }

    public function removeCompomenu(Menu $compomenu): self
    {
        if ($this->compomenu->contains($compomenu)) {
            $this->compomenu->removeElement($compomenu);
        }

        return $this;
    }

    public function getTypeFood(): ?TypeFood
    {
        return $this->typeFood;
    }

    public function setTypeFood(?TypeFood $typeFood): self
    {
        $this->typeFood = $typeFood;

        return $this;
    }

}
