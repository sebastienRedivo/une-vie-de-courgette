<?php

namespace App\Controller;
use App\Form\UpdateMdpType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;

class DashBoardController extends AbstractController
{
    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function dashboard()
    {
        return $this->render('dashboard/dashboard.html.twig');
    }

    /**
     * @Route("/member/dashboard/home", name="dashboard_home")
     */
    public function home()
    {   

        $user=$this->getUser();

        $form = $this->createForm(UpdateMdpType::class);
        return $this->render('dashboard/home.html.twig',[
            'form' => $form->createView(),
            'user' => $user
        ]);
    }
}
