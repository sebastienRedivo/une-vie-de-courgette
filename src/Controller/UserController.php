<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\UpdateInfoType;
use App\Form\UpdateMdpType;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;

class UserController extends AbstractController
{

    /**
     * @Route("/response", name="response")
     */
    public function response(User $users)
    {
        $user=$this->$users;
        return $this->render(
            'dashboard/home.html.twig',
            array('user' => $user)
        );
    }

    /**
     * @Route("/member/update/{id}/info", name="updateinfo")
     */
    public function update($id,ObjectManager $em ,Request $request ,UserRepository $userepo)
    {
        $user = $userepo->find($id); 
        // création du formulaire
        // instancie le formulaire avec les contraintes par défaut
        $form = $this->createForm(UpdateInfoType::class, $user);  
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // mise à jour du membre en base
            $em->persist($user);
            $em->flush();
 
            return $this->redirectToRoute('dashboard_home');
        }
     
            return $this->render(
                'dashboard/UpdateForm.html.twig',
                array('form' => $form->createView(),
                "form2"=>null)
            );
    }

    /**
     * 
     * @Route("/member/update/{id}/password", name="updatemdp")
     */
    public function updatemdp($id, ObjectManager $em ,Request $request ,UserRepository $userepo, UserPasswordEncoderInterface $encoder)
    {   
        $user = $userepo->find($id);

        $form = $this->createForm(UpdateMdpType::class, $user);  
        $form->handleRequest($request);
            // création du formulaire
           // instancie le formulaire avec les contraintes par défaut, + la contrainte registration pour que la saisie du mot de passe soit obligatoire
        $mdp =$request->request->get("update_mdp");
        $mdp=$mdp["password"];


        if ($form->isSubmitted()) {
    
    
                // Encode le mot de passe
                $hash = $encoder->encodePassword($user, $mdp);
            
                $user->setPassword($hash);

                // met a jour le mot de passe en base
                $em->persist($user);
                $em->flush();
    
                return $this->redirectToRoute('login');
            }
        


            return $this->render(
                'dashboard/UpdateForm.html.twig',
                array('form2' => $form->createView(),
                      'form' => null
                )
            );
    }

    /**
     * 
     * @Route("/admin/gestionuser", name="alluser")
     */
    public function GestionUser(UserRepository $repo, Request $request )
    {
        $users=$repo->findAll();

        return $this->render(
            'user/GestionUser.html.twig',
            array("users" => $users
            )
        );

    }

    /**
     * 
     * @Route("/admin/update/user/{id}", name="update_user")
     */
    public function updateUser($id, UserRepository $userRepo, Request $request, ObjectManager $om)
    {
        
        $user= $userRepo->find($id);

        if ($request->isMethod("POST"))
        {
            $role=[$request->request->get("role")];
            $user->setRoles($role);

            // met a jour le role en base
            $om->persist($user);
            $om->flush();

            return $this->redirectToRoute('alluser');
        }
        

    }

    /**
     * @Route("/admin/user/remove/{id}", name="user_remove")
     */
    public function removeCategory($id, Request $request, ObjectManager $manager,UserRepository $repo){
        
        $user = $repo->find($id);
        //permet de supprimer un utilisateur
        if($user)
            {
    $manager->remove($user);
                $manager->flush();
                return $this->redirectToRoute('alluser');
            }
    }
}



