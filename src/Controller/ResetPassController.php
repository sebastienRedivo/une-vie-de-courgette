<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\Common\Persistence\ObjectManager;
use App\Repository\UserRepository;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\Form\UpdateMdpType;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
*@Route("/reset/password/")
*/

class ResetPassController extends Controller
{
    /**
     * @Route("/resend/pass", name="resend_pass")
     */
    public function request(ObjectManager $om, UserRepository $userRepo, Request $request, \Swift_Mailer $mailer, TokenGeneratorInterface $tokenGenerator)
    {
        // création d'un formulaire "à la volée", afin que l'internaute puisse renseigner son mail
        $form = $this->createFormBuilder()
            ->add('email', EmailType::class, [
                'constraints' => [
                    new Email(),
                    new NotBlank()
                ]
            ])
            ->getForm();

        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
            $contactFormData = $form->getData();
   
            $user = $userRepo->findOneBy(array("mail" => $contactFormData['email']));

            $user->setPassword($tokenGenerator->generateToken());
           
            // aucun email associé à ce compte.
            if (!$user) {
                $request->getSession()->getFlashBag()->add('warning', "Cet email n'existe pas.");
                return $this->redirectToRoute("request_resetting");
            } 
            
            $user->setToken($tokenGenerator->generateToken());

            // enregistrement de la date de création du token
            $user->setPasswordRequestedAt(new \Datetime());
            
            $om->flush();

            $message = (new \Swift_Message("Test"))
                ->setFrom('devtestjb@gmail.com')
                ->setTo($contactFormData['email'])
                ->setBody(
                    $this->renderView(
                        // templates/emails/registration.html.twig
                        'resetpass/mail.html.twig',
                        array('user' => $user)
                    ),
                    'text/html'
                );
            
            $mailer->send($message);

            return $this->redirectToRoute("login");
        }

        return $this->render('resetpass/resetpass.html.twig', [
            'form' => $form->createView()
        ]);
    }

    // si supérieur à 10min, retourne false
    // sinon retourne false
    private function isRequestInTime(\Datetime $passwordRequestedAt = null)
    {
        if ($passwordRequestedAt === null)
        {
            return false;        
        }
        
        $now = new \DateTime();
        $interval = $now->getTimestamp() - $passwordRequestedAt->getTimestamp();

        $daySeconds = 60 * 10;
        $response = $interval > $daySeconds ? false : $reponse = true;
        return $response;
    }

    /**
     * @Route("/{id}/{token}", name="resetting")
     */
    public function resetting(Session $session, AuthenticationUtils $authUtils, User $user, $token, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        // interdit l'accès à la page si:
        // le token associé au membre est null
        // le token enregistré en base et le token présent dans l'url ne sont pas égaux
        // le token date de plus de 10 minutes
        if ($user->getToken() === null || $token !== $user->getToken() || !$this->isRequestInTime($user->getPasswordRequestedAt()))
        {
            throw new AccessDeniedHttpException();
        }

        $form = $this->createForm(UpdateMdpType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);

            // réinitialisation du token à null pour qu'il ne soit plus réutilisable
            $user->setToken(null);
            $user->setPasswordRequestedAt(null);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', "Votre mot de passe a été renouvelé.");

            return $this->redirectToRoute('login');
        }

        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();
        // set flash messages
        $session->getFlashBag()->add('notice', 'Vous étès connecté');

        return $this->render('resetpass/update.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
            'form' => $form->createView()
        )); 
    }
}

