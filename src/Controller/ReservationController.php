<?php

namespace App\Controller;

use App\Entity\HoursOpen;
use App\Entity\DateClosed;
use App\Form\HourOpenType;
use App\Entity\Reservation;
use App\Form\RechercheType;
use App\Form\DateClosedType;
use App\Form\ReservationType;
use App\Entity\MenuReservation;
use App\Repository\MenuRepository;
use App\Repository\UserRepository;
use App\Repository\HoursOpenRepository;
use App\Repository\DateClosedRepository;
use App\Repository\ReservationRepository;
use App\Repository\MenuReservationRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class ReservationController extends AbstractController
{
    /**
     * @Route("/member/reservation", name="reservation")
     */
    public function index()
    {
        return $this->render('reservation/index.html.twig', [
            'controller_name' => 'ReservationController',
        ]);
    }



    /**
     * @Route("/member/reservation/create", name="reservation_create")
     * @Route("/member/reservation/update/{id}", name="reservation_update")
     */
    public function formReservation(MenuReservation $menuResa = null, Reservation $reservation = null , Request $request, ObjectManager $manager)
    {
        if(!$reservation){
            $reservation = new Reservation();
        }
        $form = $this->createForm(ReservationType::class, $reservation);
        
        $message= 'reservation effectuer !';
  
        if ($request->isMethod('POST')) {
            
           
         $date = new \DateTime($request->request->get('reservation')['reservationAt']);
         $date->format('Y-m-d');
         $hour = new \DateTime($request->request->get('reservation')['hoursAt']);
         $hour->format('H:m');

            $reservation->setUser($this->getUser())
                        ->setReservationAt($date)
                        ->setHoursAt($hour)
                        ->setState(0)
                        ->setNbPerson($request->request->get('reservation')['nbPerson'])
                        ->setDescription($request->request->get('reservation')['description']);
                        // ->addMenuReservation($request->request->get('reservation')['menuReservations']);
            $manager->persist($reservation);
            $manager->flush();
            if(!empty($request->request->get('reservation')['menuReservations']) ){ 
            foreach( $request->request->get('reservation')['menuReservations'] as $menu ){
                
                foreach($menu as $value){
                    $menuResa = new MenuReservation();
                    $menuResa->setMenu(intval($value))
                            ->setReservation($reservation->getId());
                    $manager->persist($menuResa);
                    
                    
                }
            }  
            $manager->flush();
        }
                  
            if(!empty($message)){
                
                $request->getSession()->getFlashBag()->add('notice', $message);
            }
            
            
        } 

        return $this->render('reservation/index.html.twig', [
            
            'form' => $form->createView(),
            'edit' => $reservation->getId() !== null
        ]);
        
    }

     /**
     * @Route("/modo/reservation/show/now", name="show_reservation_now")
     */
    public function showNow(Request $request, ReservationRepository $repo, ObjectManager $manager, UserRepository $repoUser)
    {   

        $users = $repoUser->findAll();
        $date = Date('Y-m-d');

        $reservationDay= $repo->findNow($date);
        $total = implode($repo->findSum($date));
       
        return $this->render('reservation/showNow.html.twig',[
            'reservationDay' => $reservationDay,
            'total' => $total,
            'users' => $users,
          
        ]);
    }

    /** 
     *
     *  @Route("/modo/reservation/show/{id}", name="show_reservation_id")
     */
    public function showOneReservation($id, MenuRepository $menuRepo, MenuReservationRepository $repoMenuResa, Request $request, ReservationRepository $repo, UserRepository $repoUser, ObjectManager $manager)
    {
        $reservation= $repo->find($id);
        $menu= $repoMenuResa->findSumMenu($id);
        $allMenu = $repoMenuResa->findAll();
        $menus = $menuRepo->findAll();
        $users = $repoUser->findAll();
        
            return $this->render('reservation/showOne.html.twig',[
            'reservation' => $reservation,
            'users' => $users,
            'menuResa' => $menu,
            'allMenu' => $allMenu,
            'menus' => $menus
            
            ]);
        }

    /**
     * @Route("/modo/reservation/show", name="show_reservation")
     * 
     */
    public function showreservation($id=null, MenuReservationRepository $repoMenuResa, Request $request, ReservationRepository $repo, UserRepository $repoUser, ObjectManager $manager)
    {
        $reservation= $repo->findAll();
        if($id !=null){

            $menu= $repoMenuResa->findSumMenu($id);
        $resa = $repo->find($id);

        }
        
        $users = $repoUser->findAll();
        
        $form = $this->createForm(RechercheType::class);

     
    if ($request->request->count() > 0) {
            
            $dateDebut = $request->request->get('dateDebut');
            
            $dateFin= $request->request->get('dateFin');
            dump($dateDebut);
        
            $resultat = $repo->findAllReservationBetweenDate($dateDebut, $dateFin);
         
            return $this->render('reservation/show.html.twig',[
                'reservation' => null,
                'reservationDay' => null,
                'users' => $users,
                'form' => $form->createView(),
                'resultat' => $resultat,
                'menuResa' => $menu =null

                ]);
            }
            return $this->render('reservation/show.html.twig',[
            'reservation' => $reservation,
            'users' => $users,
            'resultat' => $resultat = "",
            'form' => $form->createView(),
            'menuResa' => $menu =null
            ]);
        }

    /**
     * @Route("/date_closed", name="date_closed")
     */
    public function dateClosed(Request $request, DateClosedRepository $date, UserRepository $repoUser, ObjectManager $manager)
    {
        $dateClosed= new DateClosed();
       
        $show=$date->findNext(Date('Y-m-d'));

        $form = $this->createForm(DateClosedType::class, $dateClosed);
       
        if($request->isMethod('POST')){
      
            $date = new \DateTime($request->request->get('date_closed')['date']);
                $dateClosed->setDate($date);
                $manager->persist($dateClosed);
                $manager->flush();
                
                return $this->redirectToRoute('date_closed');
        }

        return $this->render('reservation/dateClosed.html.twig',[
            'form' => $form->createView(),
            'formHour' => null,
            'show' => $show

            ]);

    }

     /**
     * @Route("/date_closed/filtre", name="date_closed_filtre")
     */
    public function dateClosedFiltre(DateClosedRepository $date)
    {
        $dates= $date->findNow(Date('Y-m-d'));
        return $dates;
    }

     /**
     * @Route("/date_closed/remove/{id}", name="date_closed_remove")
     */
    public function removeDateClosed($id, ObjectManager $manager, DateClosedRepository $date){
        $dateDelete = $date->find($id);
        if($dateDelete)
            {
                $manager->remove($dateDelete);
                $manager->flush();
                return $this->redirectToRoute('date_closed');
            }

    }

    /**
     * @Route("/hours_open", name="hours_open")
     * @Route("/hours_open/{id}", name="hours_open_update")
     */
    public function hourOpen(HoursOpen $hourOpen = null ,Request $request, HoursOpenRepository $hour, ObjectManager $manager)
    {   
        if(!$hourOpen){
            $hourOpen= new HoursOpen();
        }
        
        $show = $hour->findAll();

        $form = $this->createForm(HourOpenType::class, $hourOpen);
       
        if($request->isMethod('POST') && $form->handleRequest($request)->isValid()){
                
                $manager->persist($hourOpen);
                $manager->flush();
                return $this->redirectToRoute('hours_open');
                
        }

        return $this->render('reservation/dateClosed.html.twig',[
            'formHour' => $form->createView(),
            'show_hours' => $show
             ]);

    }

    


}
