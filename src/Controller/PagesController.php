<?php

namespace App\Controller;

use App\Form\ContactType;
use App\Repository\MenuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Repository\BlogRepository;
use App\Repository\PictureRepository;

class PagesController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home(MenuRepository $menuRepo, BlogRepository $blogRepo, PictureRepository $picRepo)
    {
        $threeLast = $blogRepo->findThreeLastBlog();
        $threeLastMenus = $menuRepo->threeLastMenus();

        return $this->render('pages/home.html.twig', [
            'threeLast' => $threeLast,
            'threeLastMenus' => $threeLastMenus
        ]);
    }

    /**
     * @Route("/resto", name="resto")
     */
    public function resto()
    {
        return $this->render('pages/resto.html.twig');
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request, \Swift_Mailer $mailer, Session $session)
    {
        $form = $this->createForm(ContactType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contactFormData = $form->getData();

            $message = (new \Swift_Message($contactFormData['subject']))
                ->setFrom($contactFormData['mail'])
                ->setTo('devtestjb@gmail.com')
                ->setBody(
                    $contactFormData['message'] . $contactFormData['mail'] . $contactFormData['phone'],
                    'text/html'
                );

            $mailer->send($message);

            $session->getFlashBag()->add('notice', 'Votre message à été envoyé');

            return $this->redirect($this->generateUrl('contact'));
        }

        return $this->render('pages/contact.html.twig', [
            'contact_form' => $form->createView(),
        ]);
    }
}
