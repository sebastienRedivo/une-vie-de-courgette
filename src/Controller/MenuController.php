<?php

namespace App\Controller;

use App\Entity\Menu;
use App\Form\MenuType;
use App\Entity\Foodtype;
use App\Entity\TypeFood;
use App\Entity\CardElement;
use App\Entity\CardCategory;
use App\Form\CardElementType;
use App\Form\FormTypeFoodType;
use App\Form\MenuCategorieType;
use App\Form\CategoryElementType;
use App\Repository\MenuRepository;
use Symfony\Component\Form\FormView;
use App\Form\FormCategoryElementType;
use App\Repository\FoodtypeRepository;
use App\Repository\TypeFoodRepository;
use Symfony\Component\Form\FormBuilder;
use App\Repository\CardElementRepository;
use App\Repository\CardCategoryRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class MenuController extends AbstractController
{

    
    /**
     * @Route("/menu", name="menu")
     * 
     */
    public function index(MenuRepository $repo, CardElementRepository $repoCard, CardCategoryRepository $repoCategorie)
    {
        $menus = $repo->findAll();
        $cards = $repoCard->findAll();
        $categories = $repoCategorie->findAll();
        return $this->render('menu/index.html.twig', [
            'cards'=> $cards,
            'cardCategory' => $categories,
            'menus'=> $menus,
        ]);
    }

    /**
     * @Route("/admin/menu", name="admin_menu")
     * 
     */
    public function indexAdmin(MenuRepository $repo)
    {
        $menus = $repo->findAll();

        return $this->render('menu/showAdmin.html.twig',[
            'menus'=> $menus,
        ]);
    }

    
    /**
     * @Route("/menu/show/{id}", name="show_menu_id")
     */
    public function showMenu($id, MenuRepository $repo, TypeFoodRepository $repoCategorie)
    {
        $menu= $repo->find($id);
         $categories= $repoCategorie->findAll();
        return $this->render('menu/show.html.twig',[
        'menu' => $menu,
        'categories' => $categories
        ]);
    }


    /**
     * @Route("/admin/menu/create", name="menu_create")
     * @Route("/admin/menu/update/{id}", name="menu_update")
     */
    public function formMenu(Menu $menu = null , Request $request, ObjectManager $manager)
    {
        if(!$menu){
            $menu = new Menu();
        }
        $form = $this->createForm(MenuType::class, $menu);
       $message= 'menu enregistrer !';
     
        if($this->validateForm($form, $manager, $request, $menu, $message)==true){
          return $this->redirectToRoute('admin_menu');
        }
      
        return $this->render('menu/create.html.twig', [
            'form' => $form->createView(),
            'edit' => $menu->getId() !== null
        ]);
    }

   
     /*****Acces aux function lié aux entité du menu*****/

    /**
     * @Route("/admin/typeFood/create", name="typeFood_create")
     * @Route("/admin/typeFood/update/{id}", name="typeFood_update")
     */
    public function addTypeFood(Foodtype $foodtype = null, Request $request, ObjectManager $manager){
        if(!$foodtype){
            $foodtype = new Foodtype();
        }
        $form = $this->createForm(FormTypeFoodType::class, $foodtype);
        if($request->isMethod('POST') && $form->handleRequest($request)->isValid())
            {
                $manager->persist($foodtype);
                $manager->flush();
                return $this->redirectToRoute('show_menu_id', ['id' => $menu->getId()]);
            }
            return $this->render('menu/create.html.twig', [
                'form' => $form->createView(),
            ]);
    }

    /**
     * @Route("/admin/categorieFood/create", name="categorieFood_create")
     * @Route("/admin/categorieFood/update/{id}", name="categorie_update")
     */
    public function addCategories(TypeFoodRepository $repoCategory, TypeFood $categorie = null, Request $request, ObjectManager $manager){
        if(!$categorie){
            $categorie= new TypeFood();
        }
        $categories= $repoCategory->findAll();
        $form = $this->createForm(FormCategoryElementType::class, $categorie);
        if($request->isMethod('POST') && $form->handleRequest($request)->isValid())
            {
                $manager->persist($categorie);
                $manager->flush();
                return $this->redirectToRoute('categorieFood_create');
            }
        return $this->render('menu/createCategoryElement.html.twig', [
            'form' => $form->createView(),
            'categories'=> $categories,
        ]);
    }

    /*****Acces aux function lié à la carte du restaurant*****/

     /**
     * @Route("/admin/card/create", name="card_create")
     * @Route("/admin/card/update/{id}", name="card_update")
     */
    public function formCard(CardElement $card = null , Request $request, ObjectManager $manager)
    {
        if(!$card){
            $card = new CardElement();
        }
        $form = $this->createForm(CardElementType::class, $card);
     
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $manager->persist($card);
            $manager->flush();
            $request->getSession()->getFlashBag()->add('notice', 'Element enregistrer !.');
            return $this->redirectToRoute('show_card_id', ['id' => $card->getId()]);
        }
        return $this->render('menu/createCard.html.twig', [
            'form' => $form->createView(),
            'edit' => $card->getId() !== null
        ]);
    }

    /**
     * @Route("modo/card/categorie/create", name="create_card_categorie")
     *  @Route("modo/card/categorie/update/{id}", name="update_card_categorie")
     */
    public function createCardCategpry(CardCategoryRepository $repoCategorie, CardCategory $category= null, Request $request, ObjectManager $manager)
    {

        if(!$category){
            $category = new CardCategory();
        }
        $categories = $repoCategorie->findAll();
        $form = $this->createForm(CategoryElementType::class, $category);

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $manager->persist($category);
            $manager->flush();
            $request->getSession()->getFlashBag()->add('notice', 'Element enregistrer !.');
            return $this->redirectToRoute('create_card_categorie');
        }
        return $this->render('menu/createCardCategorie.html.twig', [
            'form' => $form->createView(),
            'edit' => $category->getId() !== null,
            'categories'=> $categories
        ]);
    }

     /**
     * @Route("/card/show/{id}", name="show_card_id")
     */
    public function showCard($id, CardElementRepository $repo, CardCategoryRepository $repoCategorie)
    {
        $card= $repo->find($id);
        /*$foodtypes = $menu->getFoodtypes();
        */
        $categories = $repoCategorie->findAll();
        return $this->render('menu/showCard.html.twig',[
        'card' => $card,
        /*'foodtypes'=> $foodtypes
        */
        'categories'=> $categories
        ]);
    }

    /**
     * @Route("/card", name="card")
     * 
     */
    public function card(CardElementRepository $repo, CardCategoryRepository $repoCategorie)
    {
        $cards = $repo->findAll();
        $categories = $repoCategorie->findAll();
        return $this->render('menu/card.html.twig', [
            'cards'=> $cards,
            'cardCategory' => $categories
        ]);
    }


    /*****Acces aux function delete*****/
   
    /**
     * @Route("/modo/menu/remove/{id}", name="menu_remove")
     */
    public function remove($id, Request $request, ObjectManager $manager, MenuRepository $repo){

        
        $menu = $repo->find($id);
        $form = $this->get('form.factory')->create();

        if($menu)
            {
                $manager->remove($menu);
                $manager->flush();
                
                return $this->redirectToRoute('admin_menu');
            }

            return $this->render('delete.html.twig', array(
                'menu' => $menu,
                'form'   => $form->createView()
              ));
    }

    /**
     * @Route("/modo/card/remove/{id}", name="card_remove")
     */
    public function removeCard($id, Request $request, ObjectManager $manager, CardElementRepository $repo){
        $card = $repo->find($id);
        $form = $this->get('form.factory')->create();
        if($card)
            {
                $manager->remove($card);
                $manager->flush();
                return $this->redirectToRoute('card');
            }
            return $this->render('delete.html.twig', array(
                'card' => $card,
                'form'   => $form->createView()
              ));
    }

    /**
     * @Route("/modo/menu/categorie/remove/{id}", name="categorie_remove")
     */
    public function removeCategory($id, Request $request, ObjectManager $manager,TypeFoodRepository $repoCategory){
        $categorie = $repoCategory->find($id);
        $form = $this->get('form.factory')->create();
        if($categorie)
            {
                $manager->remove($categorie);
                $manager->flush();
                return $this->redirectToRoute('categorieFood_create');
            }
            return $this->render('delete.html.twig', array(
                'categorie' => $categorie,
                'form'   => $form->createView()
              ));
    }



     /****Function qui valide le formulaire evite la duplication de code et racourcis les lignes****/
     private function validateForm($form, $manager, $request, $entity, $message= null ){

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $entity->setModerate(0);
            $manager->persist($entity);
            $manager->flush();
            if(!empty($message)){
                $request->getSession()->getFlashBag()->add('notice', $message);
            }
            
            return true;
        } 
        
    }
}
