<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181008142129 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE hours_open (id INT AUTO_INCREMENT NOT NULL, hour_start TIME NOT NULL, hour_end TIME NOT NULL, id_date VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE foodtype (id INT AUTO_INCREMENT NOT NULL, type_food_id INT NOT NULL, title VARCHAR(50) NOT NULL, description VARCHAR(255) DEFAULT NULL, INDEX IDX_36D2AE6E92AFE831 (type_food_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE foodtype_menu (foodtype_id INT NOT NULL, menu_id INT NOT NULL, INDEX IDX_AC879DB97C5101B3 (foodtype_id), INDEX IDX_AC879DB9CCD7E912 (menu_id), PRIMARY KEY(foodtype_id, menu_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, firstname VARCHAR(50) DEFAULT NULL, lastname VARCHAR(50) DEFAULT NULL, username VARCHAR(50) NOT NULL, phone VARCHAR(20) DEFAULT NULL, mail VARCHAR(50) NOT NULL, password VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, adress VARCHAR(255) DEFAULT NULL, zip VARCHAR(20) DEFAULT NULL, roles JSON NOT NULL, password_requested_at DATETIME DEFAULT NULL, token VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE card_element (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, title VARCHAR(50) NOT NULL, content VARCHAR(255) NOT NULL, pricing DOUBLE PRECISION NOT NULL, INDEX IDX_165AD44F12469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE promo_code (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(50) NOT NULL, type VARCHAR(255) NOT NULL, value VARCHAR(50) NOT NULL, created_at DATETIME NOT NULL, end_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_food (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE menu (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, price DOUBLE PRECISION NOT NULL, created_at DATETIME NOT NULL, moderate TINYINT(1) NOT NULL, description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE blog (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, category_id INT NOT NULL, title VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, moderate TINYINT(1) NOT NULL, file VARCHAR(255) DEFAULT NULL, trash TINYINT(1) NOT NULL, INDEX IDX_C0155143A76ED395 (user_id), INDEX IDX_C015514312469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reservation (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, reservation_at DATE NOT NULL, hours_at TIME NOT NULL, nb_person SMALLINT NOT NULL, state TINYINT(1) NOT NULL, INDEX IDX_42C84955A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, blog_id INT NOT NULL, pseudo VARCHAR(50) NOT NULL, content LONGTEXT NOT NULL, created_at DATETIME NOT NULL, moderate TINYINT(1) NOT NULL, INDEX IDX_9474526CDAE07E97 (blog_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE menu_reservation (id INT AUTO_INCREMENT NOT NULL, menu INT NOT NULL, reservation INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE card_category (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE capacity_max_person (id INT AUTO_INCREMENT NOT NULL, weekday VARCHAR(50) NOT NULL, capacity_max INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE picture (id INT AUTO_INCREMENT NOT NULL, food_type_image_id INT DEFAULT NULL, blog_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, alt VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_16DB4F89CA932E06 (food_type_image_id), INDEX IDX_16DB4F89DAE07E97 (blog_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE date_closed (id INT AUTO_INCREMENT NOT NULL, date DATE NOT NULL, time TIME DEFAULT NULL, UNIQUE INDEX UNIQ_E0737207AA9E377A (date), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE foodtype ADD CONSTRAINT FK_36D2AE6E92AFE831 FOREIGN KEY (type_food_id) REFERENCES type_food (id)');
        $this->addSql('ALTER TABLE foodtype_menu ADD CONSTRAINT FK_AC879DB97C5101B3 FOREIGN KEY (foodtype_id) REFERENCES foodtype (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE foodtype_menu ADD CONSTRAINT FK_AC879DB9CCD7E912 FOREIGN KEY (menu_id) REFERENCES menu (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE card_element ADD CONSTRAINT FK_165AD44F12469DE2 FOREIGN KEY (category_id) REFERENCES card_category (id)');
        $this->addSql('ALTER TABLE blog ADD CONSTRAINT FK_C0155143A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE blog ADD CONSTRAINT FK_C015514312469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C84955A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CDAE07E97 FOREIGN KEY (blog_id) REFERENCES blog (id)');
        $this->addSql('ALTER TABLE picture ADD CONSTRAINT FK_16DB4F89CA932E06 FOREIGN KEY (food_type_image_id) REFERENCES foodtype (id)');
        $this->addSql('ALTER TABLE picture ADD CONSTRAINT FK_16DB4F89DAE07E97 FOREIGN KEY (blog_id) REFERENCES blog (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE blog DROP FOREIGN KEY FK_C015514312469DE2');
        $this->addSql('ALTER TABLE foodtype_menu DROP FOREIGN KEY FK_AC879DB97C5101B3');
        $this->addSql('ALTER TABLE picture DROP FOREIGN KEY FK_16DB4F89CA932E06');
        $this->addSql('ALTER TABLE blog DROP FOREIGN KEY FK_C0155143A76ED395');
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C84955A76ED395');
        $this->addSql('ALTER TABLE foodtype DROP FOREIGN KEY FK_36D2AE6E92AFE831');
        $this->addSql('ALTER TABLE foodtype_menu DROP FOREIGN KEY FK_AC879DB9CCD7E912');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CDAE07E97');
        $this->addSql('ALTER TABLE picture DROP FOREIGN KEY FK_16DB4F89DAE07E97');
        $this->addSql('ALTER TABLE card_element DROP FOREIGN KEY FK_165AD44F12469DE2');
        $this->addSql('DROP TABLE hours_open');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE foodtype');
        $this->addSql('DROP TABLE foodtype_menu');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE card_element');
        $this->addSql('DROP TABLE promo_code');
        $this->addSql('DROP TABLE type_food');
        $this->addSql('DROP TABLE menu');
        $this->addSql('DROP TABLE blog');
        $this->addSql('DROP TABLE reservation');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE menu_reservation');
        $this->addSql('DROP TABLE card_category');
        $this->addSql('DROP TABLE capacity_max_person');
        $this->addSql('DROP TABLE picture');
        $this->addSql('DROP TABLE date_closed');
    }
}
