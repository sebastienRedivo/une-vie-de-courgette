<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateInfoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class,array(
                "label" => "Prénom",
                "required" => false,
            ))
            ->add('lastname', TextType::class, array(
                "label" => "Nom",
                "required" => false,
            ))
            ->add('phone', TelType::class, array(
                "label" => "Numéro de téléphone",
                "attr"  => array("maxlenght" => "10"),
                "required" => false,
            ))
            ->add('adress', TextType::class, array(
                "label" => "adresse & numéro de voix",
                "required" => false,
            ))
            ->add('zip', TextType::class, array(
                "label" => "code postal",
                "attr"  => array("maxlenght" => "5"),
                "required" => false,
            ))
            ->add("submit", SubmitType::class, [
                'label' => 'Mettre à jour',
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
