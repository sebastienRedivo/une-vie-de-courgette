<?php

namespace App\Form;

use App\Entity\Blog;
use App\Entity\Category;
use App\Entity\Picture;
use Serializable;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class BlogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' =>'Titre',
                'attr' => [
                    'placeholder' => "Titre de l'article"
                ]
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Contenu',
                'attr' => [
                    'placeholder' => "Contenu de l'article", 'class' => 'ckeditor'
                ]
            ])
            ->add('category', EntityType::class, [
                'label' => 'catégrorie',
                'class'        => Category::class,
                'choice_label' => 'name',
                'multiple'     => false
            ])
            ->add('file', FileType::class, [
                "label" => "Images de l'article",
                'data_class'=> null,
                'multiple' => true,
                'required' => false,
                'empty_data' => null
            ])
            ->add('submit', SubmitType::class, [
                'label' => "Envoyer",
                'attr' => [
                    'class' => "btn btn-primary"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Blog::class
        ]);
    }
}
