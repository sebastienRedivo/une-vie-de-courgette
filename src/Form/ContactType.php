<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [ 'label' =>'Prénom', 'attr' => ['placeholder' => 'Votre prénom']])
            ->add('lastname', TextType::class, [ 'label' =>'Nom', 'attr' => ['placeholder' => 'Votre nom']])
            ->add('mail', EmailType::class, [ 'label' =>'Email', 'attr' => ['placeholder' => 'Votre adresse mail']])
            ->add('phone', TelType::class, [ 'label' =>'Téléphone', 'attr' => ['placeholder' => 'Votre numéro de téléphone']])
            ->add('subject', TextType::class, [ 'label' =>'Sujet', 'attr' => ['placeholder' => 'Sujet de votre message']])
            ->add('message', TextareaType::class, [ 'label' =>'Votre message', 'attr' => ['placeholder' => 'Contenu de votre message']])
            ->add('Envoyer', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
