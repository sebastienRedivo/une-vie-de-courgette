<?php

namespace App\Repository;

use App\Entity\MenuReservation;
use App\Repository\MenuRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method MenuReservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method MenuReservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method MenuReservation[]    findAll()
 * @method MenuReservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MenuReservationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MenuReservation::class);
    }



    public function findSumMenu($resa)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.reservation = :val')
            ->setParameter('val', $resa)
            ->select('count(r.menu)')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

//    /**
//     * @return MenuReservation[] Returns an array of MenuReservation objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MenuReservation
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
