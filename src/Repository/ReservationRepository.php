<?php

namespace App\Repository;

use App\Entity\Reservation;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Reservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reservation[]    findAll()
 * @method Reservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReservationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Reservation::class);
    }

//    /**
//     * @return Reservation[] Returns an array of Reservation objects
//     */
    
    public function findSum($dateDebut, $dateFin = null)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.reservationAt = :val')
            ->setParameter('val', $dateDebut)
            ->select('SUM(r.nbPerson)')
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findNow($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.reservationAt = :val')
            ->setParameter('val', $value)
            ->orderBy('r.hoursAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }
    
        public function findAllReservationBetweenDate( $dateDebut, $dateFin): array
    {
        // automatically knows to select Products
        // the "p" is an alias you'll use in the rest of the query $dateFin = date('Y-m-d')
       
        
        $qb = $this->createQueryBuilder('reservation')
            ->where('reservation.reservationAt > :dateDebut')
            ->andWhere('reservation.reservationAt < :dateFin')
            ->setParameter('dateDebut' , $dateDebut )
            ->setParameter('dateFin', $dateFin)
            ->orderBy('reservation.reservationAt', 'ASC')
            ->getQuery();
            
       return $qb->getResult();
        
        // to get just one result:
        // $product = $qb->setMaxResults(1)->getOneOrNullResult();
    }
    /*
    public function findOneBySomeField($value): ?Reservation
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
