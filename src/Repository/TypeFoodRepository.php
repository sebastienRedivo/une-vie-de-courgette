<?php

namespace App\Repository;

use App\Entity\TypeFood;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TypeFood|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeFood|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeFood[]    findAll()
 * @method TypeFood[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeFoodRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TypeFood::class);
    }

//    /**
//     * @return TypeFood[] Returns an array of TypeFood objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeFood
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
