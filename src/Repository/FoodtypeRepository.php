<?php

namespace App\Repository;

use App\Entity\Foodtype;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Foodtype|null find($id, $lockMode = null, $lockVersion = null)
 * @method Foodtype|null findOneBy(array $criteria, array $orderBy = null)
 * @method Foodtype[]    findAll()
 * @method Foodtype[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FoodtypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Foodtype::class);
    }

//    /**
//     * @return Foodtype[] Returns an array of Foodtype objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Foodtype
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
