<?php

namespace App\Repository;

use App\Entity\CapacityMaxPerson;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CapacityMaxPerson|null find($id, $lockMode = null, $lockVersion = null)
 * @method CapacityMaxPerson|null findOneBy(array $criteria, array $orderBy = null)
 * @method CapacityMaxPerson[]    findAll()
 * @method CapacityMaxPerson[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CapacityMaxPersonRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CapacityMaxPerson::class);
    }

//    /**
//     * @return CapacityMaxPerson[] Returns an array of CapacityMaxPerson objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CapacityMaxPerson
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
