# I Mettre en place le projet sur votre machine locale

Pour faire fonctioner le site sur votre machine local, vous devez avoir accès
aux composants suivants :

- Le gestionnaire de paquets COMPOSER
- Un compte sur gitlabs
- Un éditeur de texte
- PHP version > 7.1
- PHPMYADMIN
- Un accès à votre terminal
- Git


    Procédure :

    1. Ouvrez votre terminal et créez un dossier sur votre serveur local
        $ mkdir <Nom du dossier>

    2. Initialiser votre clef SSH et synchroniser la avec votre compte gitlabs
        $ suivez les instruction de se lien : https://docs.gitlab.com/ee/ssh/

    3. Placez vous dans le dossier du site
        $ cd courgette

    4. Téléchargez les dépandances du framework
        $ composer install

    5. Configurer si besoin le fichier .env avec vos identifiants phpmyadmin et configurer swiftmailer
        $ DATABASE_URL=mysql://<votre nom d'utilisateur>:<votre mot de passe>@127.0.0.1:3306/<le nom de votre base de données>

    6. Créez la base de données
        $ php bin/console doctrine:database:create

    7. Créez une migration
      $ php bin/console make:migration

    7. Lancez les migrations pour récupérer les tables de la base de données
        $ php bin/console doctrine:migrations:migrate

    8. (optionel) Lancer les fixtures pour remplir la base de données avec des données de test
        $ php bin/console doctrine:fixtures:load

    9. Lancer le serveur local php
        $ php bin/console server:run

    10. Rendez-vous à l'adresse localhost affichée dans votre terminal

# II déployer le site en porduction en ligne

    Procédure :

    1. Télécharger les fichiers de la branche master sur votre serveur via le FTP de votre hébèrgeur web

    2. (optionel) Pour voir l'appli avec les données de test, accédez à votre interface PhpMyAdmin et          importez le fichier "db_courgette.sql.zip".
       Note: inutile de créer une nouvelle base le script SQL s'en occupe pour vous

    3. Importez la base de données vierge du site, accédez à votre interface PhpMyAdmin et importez le fichier "courgette.sql.zip".
       Note: inutile de créer une nouvelle base le script SQL s'en occupe pour vous

    5. Configurer le fichier .env avec vos identifiants phpmyadmin 
        $ DATABASE_URL=mysql://<votre nom d'utilisateur>:<votre mot de passe>@<domaine de votre BDD>/<le nom de votre base de données>
    
    6. Configurer le fichier .env avec vos identifiants gmail 
        $ MAILER_URL=gmail://<votre nom d'utilisateur gmail>:<votre mot de passe>@gmail.com

    6. Enjoy !

# III Procédure pour le versioning

    Procédure :

    1. Placez vous sur la branche dev
        $ git checkout dev

    2. Récupérez les dernières mises à jour du projet
        $ git pull origin dev

    3. Créez votre branche pour ajouter une fonctionalité en suivant cette norme
        $ git branch <initiales prénom><nom fonctionalité><date format = 120918>

    4. Faite vos commits à chaques avancées majeurs
        $ git add .
        $ git commit -m "<message de commit décrivant la fonctionalité>"

    5. Une fois votre travail terminé envoyez votre branche sur gitLabs
        $ git push origin <nom de votre branche>

    6. Faite une demande de merge request sur votre espace gitLabs
        $ Naviguez vers l'onget <demandes de fusion>
        $ Faite une demande de fusion en attribuant la tache à <thery jean-Baptiste>
        $ ! Changez la branche de destination de MASTER à DEV!
        $ Cochez l'onget <supprimer la branche une fois la fusion accépter>

# IV Note importante

    1. Ne pas oublier de lancer les fixtures pour le jour de la présentations
    2. identifiants
    	user :
    		mail: 	testuser@test.fr
    		pseudo: testuser
    		mdp:	azerty

    3. Si vous rencontrez un problème avec la commande "composer install" en
    rapport avec la memoire de php, merci d'augmenter la 'memory-limit' à "-1"
    (allocation de mémoire illimitée )de votre php.ini





