  $(document).ready(function() {
 

      /** on genere une variable qui vas contenir les dates de fermeture */
     var disableddates =[];
     $.ajax({
         type: 'post',
         url: 'http://127.0.0.1:8000/date_closed/filtre',
         success: function(msg){
             // on converti le JSON en string
             var json = JSON.stringify(msg); 
             // on parse le JSON     
             json = JSON.parse(json);
             // on ajoute dans le tableau les dates presente dans la BDD
             for( var i = 0 ; i<json.length; i++){
                 // on recupere seulement la date car il y a le time à 00:00:0000
                 // je lui precise donc de prendre que les premiers élements 
                 disableddates[i] = json[i].date.date.substr(0, 10);
             }
         }
 
     })
 
     function DisableSpecificDates(date) {
         var m = date.getMonth() + 1;
             m= (m < 10 ? '0' : '') + m;  
         var d = date.getDate();
             d= (d < 10 ? '0' : '') + d;
         var y = date.getFullYear();
         var currentdate =  y + '-' + m + '-' + d ;
         console.log(currentdate);
 
     // on parcour le tableau qui contient les jours de fermeture
         for (var i = 0; i < disableddates.length; i++) {
         
         // si la date est présente dans le tableau on la desactive. 
             if ($.inArray(currentdate, disableddates) != -1 ) {
             return [false];
             } 
         }
     
     // si la date n'est pas présente dans le tableau on verifie si c'est un week-end
     // on utilise la function noWeekends
     var weekenddate = $.datepicker.noWeekends(date);
     return weekenddate; 
     
     }
     
     // on passe le calendrier en francais
     $(function() {
         $( ".datepicker" ).datepicker({
             closeText: 'Fermer',
                 prevText: 'Précédent',
                 nextText: 'Suivant',
                 currentText: 'Aujourd\'hui',
                 monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                 monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                 dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                 dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                 dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                 weekHeader: 'Sem.',
                 dateFormat: 'yy-mm-dd',
                 minDate : 0,
                 beforeShowDay: DisableSpecificDates
         });
     });
 // affiche une tranche horaire de reservation
     $('.timepicker').timepicker({ 
             timeFormat: 'H:i',
             minTime: '11:00', // 11:45:00 AM,
             maxTime: '14:00' });
     $('#setTimeButton').on('click', function (){
         $('.timepicker').timepicker('setTime', new Date());
     });
     
 
 });


