<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MenuReservationRepository")
 */
class MenuReservation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $menu;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $reservation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMenu(): ?int
    {
        return $this->menu;
    }

    public function setMenu(Int $menu): self
    {
        $this->menu = $menu;

        return $this;
    }

    public function getReservation(): ?int
    {
        return $this->reservation;
    }

    public function setReservation(Int $reservation): self
    {
        $this->reservation = $reservation;

        return $this;
    }
}
