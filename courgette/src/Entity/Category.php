<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Blog", mappedBy="category")
     */
    private $blogs;

    public function __construct()
    {
        $this->blogs = new ArrayCollection();
        $this->cardElements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Blog[]
     */
    public function getBlogs(): Collection
    {
        return $this->blogs;
    }

    public function addBlog(Blog $blog): self
    {
        if (!$this->blogs->contains($blog)) {
            $this->blogs[] = $blog;
            $blog->setCategory($this);
        }

        return $this;
    }

    public function removeBlog(Blog $blog): self
    {
        if ($this->blogs->contains($blog)) {
            $this->blogs->removeElement($blog);
            // set the owning side to null (unless already changed)
            if ($blog->getCategory() === $this) {
                $blog->setCategory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CardElement[]
     */
    public function getCardElements(): Collection
    {
        return $this->cardElements;
    }

    public function addCardElement(CardElement $cardElement): self
    {
        if (!$this->cardElements->contains($cardElement)) {
            $this->cardElements[] = $cardElement;
            $cardElement->setCategory($this);
        }

        return $this;
    }

    public function removeCardElement(CardElement $cardElement): self
    {
        if ($this->cardElements->contains($cardElement)) {
            $this->cardElements->removeElement($cardElement);
            // set the owning side to null (unless already changed)
            if ($cardElement->getCategory() === $this) {
                $cardElement->setCategory(null);
            }
        }

        return $this;
    }
}
