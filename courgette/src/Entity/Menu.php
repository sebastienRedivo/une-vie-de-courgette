<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MenuRepository")
 */
class Menu
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Foodtype", cascade={"persist"}, mappedBy="compomenu")
     */
    private $foodtypes;

    /**
     * @ORM\Column(type="boolean")
     */
    private $moderate = 0;

    public $img;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MenuReservation", mappedBy="menu")
     */
    private $reservationMenu;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;


    public function __construct()
    {   $this->createdAt = new \Datetime();
        $this->foodtypes = new ArrayCollection();
        $this->reservationMenu = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt= $createdAt;

        return $this;
    }

    /**
     * @return Collection|Foodtype[]
     */
    public function getFoodtypes(): Collection
    {
        return $this->foodtypes;
    }

    public function addFoodtype(Foodtype $foodtype): self
    {
        if (!$this->foodtypes->contains($foodtype)) {
            $this->foodtypes[] = $foodtype;
            $foodtype->addCompomenu($this);
        }

        return $this;
    }

    public function removeFoodtype(Foodtype $foodtype): self
    {
        if ($this->foodtypes->contains($foodtype)) {
            $this->foodtypes->removeElement($foodtype);
            $foodtype->removeCompomenu($this);
        }

        return $this;
    }

    public function getModerate(): ?bool
    {
        return $this->moderate;
    }

    public function setModerate(bool $moderate): self
    {
        $this->moderate = $moderate;

        return $this;
    }

    /**
     * @return Collection|reservationMenu[]
     */
    public function getReservationMenu(): Collection
    {
        return $this->reservationMenu;
    }

    public function addReservation(MenuReservation $reservationMenu): self
    {
        if (!$this->reservationMenu->contains($reservationMenu)) {
            $this->reservationMenu[] = $reservationMenu;
            $reservationMenu->setMenu($this);
        }

        return $this;
    }

    public function removeReservation(MenuReservation $reservationMenu): self
    {
        if ($this->reservationMenu->contains($reservationMenu)) {
            $this->reservationMenu->removeElement($reservationMenu);
            // set the owning side to null (unless already changed)
            if ($reservationMenu->getMenu() === $this) {
                $reservationMenu->setMenu(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

}
