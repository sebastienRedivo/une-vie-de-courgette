<?php

namespace App\Services;

use App\Repository\BlogRepository;
use App\Repository\CategoryRepository;
use App\Repository\CommentRepository;
use App\Repository\PictureRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;

class RetreiveDashboardData
{
    private $blogRepo;
    private $picRepo;
    private $catRepo;
    private $userRepo;
    private $commentRepo;
    private $manager;

    public function __construct(
        BlogRepository $blogRepo,
        PictureRepository $picRepo,
        CategoryRepository $catRepo,
        UserRepository $userRepo,
        CommentRepository $commentRepo,
        ObjectManager $manager
    )
    {
        $this->blogRepo = $blogRepo;
        $this->picRepo = $picRepo;
        $this->catRepo = $catRepo;
        $this->userRepo = $userRepo;
        $this->commentRepo = $commentRepo;
        $this->manager = $manager;
    }

    public function nbBlogs()
    {
        return $nbBlogs = $this->blogRepo->count(['moderate' => 1, 'trash' => 0]);
    }

    public function nbTrashed()
    {
        return $nbTrashed = $this->blogRepo->count(['trash' => 1]);
    }

    public function nbBlogsModerate()
    {
        return $nbBlogsModerate = $this->blogRepo->count(['moderate' => 0, 'trash' => 0]);
    }

    public function allBlogs()
    {
        return $blog = $this->blogRepo->findBY(['moderate' => 1], ['createdAt' => 'DESC']);
    }

    public function allPictures()
    {
        return $pictures = $this->picRepo->findAll();
    }

    public function allCategorys()
    {
        return $categorys = $this->catRepo->findAll();
    }

    public function allComments()
    {
        return $comments = $this->commentRepo->findBY(['moderate' => 1], ['createdAt' => 'DESC']);
    }

    public function allUsers()
    {
        return $users = $this->userRepo->findAll();
    }
}