<?php

namespace App\Controller;

use App\Form\RegisterType;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Repository\RoleRepository;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Form\ContactType;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
 
    
class RegisterController extends Controller
{
        /**
         * @Route("/inscription", name="user_registration")
         */
        public function registerAction( \Swift_Mailer $mailer, Request $request, UserPasswordEncoderInterface $encoder, ObjectManager $em)
        {
            // création du formulaire
            $user = new User();
            // instancie le formulaire avec les contraintes par défaut, + la contrainte registration pour que la saisie du mot de passe soit obligatoire
            $form = $this->createForm(RegisterType::class, $user);  
            $form->handleRequest($request);
            $role= ["ROLE_USER"];
            
            if ($form->isSubmitted() && $form->isValid()) 
            {
            
                // Encode le mot de passe
                $hash = $encoder->encodePassword($user, $user->getPassword());
    
                $user->setPassword($hash);
                //definie le role 
                $user->setRoles($role);

                // Enregistre le membre en base
                $em->persist($user);
                $em->flush();
                //redirige vers la page de connexion
                return $this->redirectToRoute('login');
            }

            //crée la vue du formulaire
            return $this->render(
            'register/register.html.twig',
            array('form' => $form->createView())
                );    
        }

    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authUtils, Session $session)
    {
        // obtenir l'erreur de connexion s'il y en a une
        $error = $authUtils->getLastAuthenticationError();
        // dernier nom d'utilisateur entré par l'utilisateur

        $lastUsername = $authUtils->getLastUsername();
        // set flash messages
        $session->getFlashBag()->add('notice', 'Vous étes maintenant connecté');

        return $this->render('register/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(Session $session) 
    {
        // set flash messages
        $session->getFlashBag()->add('notice', 'vous avez été deconnecter');
    }
}