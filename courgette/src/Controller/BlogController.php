<?php

namespace App\Controller;

use App\Entity\Picture;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Entity\Blog;
use App\Entity\Comment;
use App\Repository\BlogRepository;
use App\Repository\CategoryRepository;
use App\Repository\CommentRepository;
use App\Repository\PictureRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Form\CommentType;
use App\Form\BlogType;

class BlogController extends AbstractController
{
    private $blogRepo;
    private $picRepo;
    private $catRepo;
    private $userRepository;
    private $commentRepo;
    private $manager;

    public function __construct(
        BlogRepository $blogRepo,
        PictureRepository $picRepo,
        CategoryRepository $catRepo,
        UserRepository $userRepository,
        CommentRepository $commentRepo,
        ObjectManager $manager
    )
    {
        $this->blogRepo = $blogRepo;
        $this->picRepo = $picRepo;
        $this->catRepo = $catRepo;
        $this->userRepository = $userRepository;
        $this->commentRepo = $commentRepo;
        $this->manager = $manager;
    }

    /**
     * @Route("/blog/{page}", name="blog_index", requirements={"page"="\d+"})
     */
    public function index($page = null)
    {
        $nbPerPage = 4;
        $blog = $this->blogRepo->getBlogs($page, $nbPerPage);
        $threeLast = $this->blogRepo->findThreeLastBlog(['trash' => 0, 'moderate' => 1]);
        $nbPages = ceil(count($blog) / $nbPerPage);

        return $this->render('blog/index.html.twig', [
            'blog' => $blog,
            'threeLast' => $threeLast,
            'nbPages' => $nbPages,
            'page' => $page
        ]);
    }

    /**
     * @Route ("/modo/blog/index/{page}", name="blog_admin_index", requirements={"page"="\d+"})
     ** @Route ("/modo/blog/moderate/index/{page}", name="blog_moderate_index")
     *** @Route ("/modo/blog/moderate/{id}", name="blog_moderate", requirements={"id"="\d+"}, requirements={"page"="\d+"})
     */
    public function adminIndex($page = null, $id = null, Session $session, Request $request)
    {
        $nbPerPage = 6;
        $listBlogs = $this->blogRepo->getBlogs($page, $nbPerPage);
        $listBlogsModerate = $this->blogRepo->getBlogsModerate($page, $nbPerPage);
        $nbPages = ceil(count($listBlogs) / $nbPerPage);

        if ($request->attributes->get('_route') == "blog_moderate_index") {
            $nbPages = ceil(count($listBlogsModerate) / $nbPerPage);
            return $this->render('blog/admin/moderate_blog.html.twig', [
                'blogsModerate' => $listBlogsModerate,
                'nbPages' => $nbPages,
                'page' => $page
            ]);
        }

        if ($request->attributes->get('_route') == "blog_moderate") {
            $blog = $this->blogRepo->findOneBy(['id' => $id]);
            $blog->setModerate(1);
            $this->manager->persist($blog);
            $this->manager->flush();

            $session->getFlashBag()->add("notice", "L'article à été modérer");

            return $this->redirectToRoute("blog_admin_index", ['page' => 1]);
        }

        return $this->render('blog/admin/index.html.twig', [
            'listBlogs' => $listBlogs,
            'nbPages' => $nbPages,
            'page' => $page
        ]);
    }

    /**
     * @Route ("/edit/blog/new", name="blog_create")
     ** @Route ("/edit/blog/update/{id}", name="blog_update", requirements={"id"="\d+"})
     */
    public function create($id = null, Blog $blog = null, Request $request, Session $session)
    {
        $blogPictures = $this->picRepo->findBy(['blog' => $id]);

        if (!$blog) {
            $blog = new Blog();
        }
        if ($id) {
            $titleForm = "Modifier un article";
            $titleImage = "Images de l'article";
        } else {
            $titleForm = "Créer un article";
            $titleImage = "";
        }

        $form = $this->createForm(BlogType::class, $blog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $files = $form->get('file')->getData();
            if ($files) {
                foreach ($files as $file) {
                    $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                    $file->move($this->getParameter('images_directory'), $fileName);

                    $pictures = new Picture();
                    $pictures
                        ->setName($fileName)
                        ->setBlog($blog)
                        ->setAlt($file->getClientOriginalName());

                    $blog->setfile($file);
                    $this->manager->persist($pictures);
                }
            }

            $blog->setModerate(0);
            $blog->setUser($this->getUser());

            switch ($request->attributes->get('_route')) {
                case "blog_update":
                    $blog->setUpdatedAt(new \DateTime());
                    $session->getFlashBag()->add("notice", "L'article à été mis a jours");
                    break;
                case "blog_create":
                    $blog->setCreatedAt(new \DateTime());
                    $session->getFlashBag()->add("notice", "L'article à été envoyé à la modération");
                    break;
            }

            $this->manager->persist($blog);
            $this->manager->flush();

            return $this->redirectToRoute("blog_moderate_index", ['page' => 1]);
        }

        return $this->render('blog/admin/create.html.twig', [
            'formBlog' => $form->createView(),
            'titleForm' => $titleForm,
            'titleImage' => $titleImage,
            'blogPictures' => $blogPictures
        ]);
    }


    /**
     * @Route ("/modo/blog/trash/index", name="blog_trash_index")
     ** @Route ("/modo/blog/trash/{id}", name="blog_trash", requirements={"id"="\d+"})
     *** @Route ("/modo/blog/untratch/{id}", name="blog_untrash", requirements={"id"="\d+"})
     */
    public function trash($id = null, Blog $blog = null, Request $request, Session $session)
    {
        $blogsTrashed = $this->blogRepo->findBy(['trash' => 1]);
        $blog = $this->blogRepo->findOneBy(['id' => $id]);

        if ($request->attributes->get('_route') == "blog_trash") {
            $blog->setTrash(1);
            $this->manager->persist($blog);
            $this->manager->flush();

            $session->getFlashBag()->add("notice", "L'article à été mis à la corbeille");

            return $this->redirectToRoute("blog_admin_index", ['page' => 1]);
        }

        if ($request->attributes->get('_route') == "blog_untrash") {
            $blog->setTrash(0);
            $this->manager->persist($blog);
            $this->manager->flush();

            $session->getFlashBag()->add("notice", "L'article à été enlevé de la corbeille");

            return $this->redirectToRoute("blog_admin_index", ['page' => 1]);
        }

        return $this->render("blog/admin/trash.html.twig", [
            'blogsTrashed' => $blogsTrashed
        ]);
    }

    /**
     * @Route ("/modo/blog/delete/{id}", name="blog_delete", requirements={"id"="\d+"});
     ** @Route ("/modo/blog/delete/onepicture/{picId}/{id}", name="blog_delete_photo");
     */
    public function delete($id= null, $picId = null, Blog $blog, Session $session, Request $request)
    {
        switch ($request->attributes->get('_route')) {
            case "blog_delete_photo":
                $picture = $this->picRepo->findOneBy(['id' => $picId]);
                $this->manager->remove($picture);
                $this->manager->flush();

                return $this->redirectToRoute("blog_update", ['id' => $id]);
                break;
            case "blog_delete":
                $pictures = $this->picRepo->findBy(['id' => $id]);
                foreach ($pictures as $picture) {
                    $this->manager->remove($picture);
                    $this->manager->flush();
                }

                $blog = $this->blogRepo->findOneBy(['id' => $id]);
                $this->manager->remove($blog);
                $this->manager->flush();

                $session->getFlashBag()->add("notice", "L'article à été définitivement supprimer");
                break;
        }

        return $this->redirectToRoute("blog_trash_index");
    }

    /**
     * @Route ("/blog/article/{id}", name="blog_show", requirements={"id"="\d+"})
     */
    public function show(Session $session, Request $request, Blog $blog, $id)
    {
        $comments = $this->commentRepo->findBy(['blog' => $id], ['createdAt' => 'DESC']);
        $form = $this->createForm(CommentType::class);
        $comment = new Comment();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment = $form->getData();
            $comment->setModerate(1);
            $comment->setCreatedAt(new \DateTime());
            $comment->setBlog($blog);

            $this->manager->persist($comment);
            $this->manager->flush();

            $session->getFlashBag()->add('notice', 'Votre commentaire à été envoyé');

            return $this->redirectToRoute('blog_show', [
                'id' => $id
            ]);
        }

        return $this->render('blog/show.html.twig', [
            'comment_form' => $form->createView(),
            'blog' => $blog,
            'comments' => $comments
        ]);
    }

    /**
     * @Route ("/blog/category/{id}", name="blog_show_cat", requirements={"id"="\d+"})
     */
    public function showByCat($id)
    {
        $blog = $this->blogRepo->findBy(['category' => $id, 'trash' => 0, 'moderate' => 1]);
        $categoryTitle = $this->catRepo->findOneBy(['id' => $id]);

        return $this->render('blog/show_cat.html.twig', [
            'blog' => $blog,
            'categoryTitle' => $categoryTitle
        ]);
    }
}
