<?php

namespace App\Repository;

use App\Entity\CardElement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CardElement|null find($id, $lockMode = null, $lockVersion = null)
 * @method CardElement|null findOneBy(array $criteria, array $orderBy = null)
 * @method CardElement[]    findAll()
 * @method CardElement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CardElementRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CardElement::class);
    }

//    /**
//     * @return CardElement[] Returns an array of CardElement objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CardElement
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
