<?php

namespace App\Repository;

use App\Entity\DateClosed;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method DateClosed|null find($id, $lockMode = null, $lockVersion = null)
 * @method DateClosed|null findOneBy(array $criteria, array $orderBy = null)
 * @method DateClosed[]    findAll()
 * @method DateClosed[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DateClosedRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DateClosed::class);
    }

    public function findNext($value)
    {
          return $this->createQueryBuilder('r')
            ->andWhere('r.date >= :val')
            ->setParameter('val', $value)
            ->orderBy('r.date', 'ASC')
            ->getQuery()
            ->getResult();
        

           
    }

    public function findNow($value)
    {
          $qb=$this->createQueryBuilder('r')
            ->andWhere('r.date >= :val')
            ->setParameter('val', $value)
            ->orderBy('r.date', 'ASC')
            ->getQuery();
         $json = $qb->getArrayResult();
        

            return new JsonResponse($json);
    }
//    /**
//     * @return DateClosed[] Returns an array of DateClosed objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DateClosed
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
