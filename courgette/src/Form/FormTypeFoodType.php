<?php

namespace App\Form;

use App\Form\MenuType;
use App\Entity\Foodtype;
use App\Entity\TypeFood;
use App\Form\FormCategoryElementType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class FormTypeFoodType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('typeFood', EntityType::class, [
                'class' => TypeFood::class,
                'choice_label' => 'title',
                'multiple' => false,
                'label' => 'Catégorie'
            ])
            ->add('title', TextType::class, [
                'label' => 'Titre',
                    'attr' => [
                        "placeholder' => 'Nom de l'élément",
                    ]
                ])
            ->add('description', TextType::class, [
                'attr' => [
                        'placeholder' => 'pomme de terre, lardons, oignons, créme...',
                    ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Foodtype::class,
        ]);
    }
}
