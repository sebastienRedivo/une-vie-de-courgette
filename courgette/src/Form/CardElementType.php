<?php

namespace App\Form;

use App\Entity\CardElement;
use App\Entity\CardCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CardElementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('category', EntityType::class, array(
            'class'        => CardCategory::class,
            'choice_label' => 'title',
            'multiple'     => false
        ))
            ->add('title')
            ->add('content', TextareaType::class)
            ->add('pricing')
            
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CardElement::class,
        ]);
    }
}
