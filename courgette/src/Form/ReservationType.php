<?php

namespace App\Form;

use App\Entity\Menu;
use App\Form\MenuType;
use App\Form\MenuListType;
use App\Entity\Reservation;
use App\Form\RelationMenuResaType;
use Symfony\Component\Form\AbstractType;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;



class ReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', TextareaType::class,['required' => false])
            ->add('reservationAt', TextType::class,['attr'=>['class'=> 'datepicker']])
            ->add('hoursAt', TextType::class,['attr'=>['class'=> 'timepicker']])
            ->add('nbPerson')
            ->add('menuReservations', CollectionType::class, array(
                'entry_type'   => RelationMenuResaType::class,
                'allow_add'    => true,
                'allow_delete' => true,
                'by_reference' => false
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class,
        ]);
    }
}
