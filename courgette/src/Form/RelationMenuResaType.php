<?php

namespace App\Form;

use App\Entity\Menu;
use App\Entity\MenuReservation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RelationMenuResaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('menu', EntityType::class, array(
                'label' => false,
                'class'        => Menu::class,
                'choice_label' => 'name',
                'multiple'     => false
                ))
            // ->add('reservation')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MenuReservation::class,
        ]);
    }
}
