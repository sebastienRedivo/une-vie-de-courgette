<?php

namespace App\Form;

use App\Entity\Menu;
use App\Entity\TypeFood;
use App\Entity\Foodtype;
use App\Form\FormTypeFoodType;
use App\Form\FormCategoryElementType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class MenuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Titre du menu'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description',
                'attr' => [
                    'placeholder' => "Contenu de l'article", 'class' => 'ckeditor'
                ]
            ])
            ->add('price', MoneyType::class, [
                'label' => 'Prix du menu'
            ])
            ->add('foodtypes', CollectionType::class, array(
                'entry_type' => FormTypeFoodType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => " ",
                'attr' => [
                    'class' => 'd-flex flex-wrap justify-content-around'
                ]
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Menu::class,
        ]);
    }
}
