<?php

namespace App\DataFixtures;

use App\Entity\Foodtype;
use App\Entity\Menu;
use App\Entity\TypeFood;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class MenuFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');

        $foodtypeTitles = ["Salade", "Steak frite", "Glace"];

        for ($i = 0; $i <= 2; $i++){
            $menu = new Menu();
            $menu
                ->setName("Menu de test $i")
                ->setDescription('Reiciendis tempore tempore magnam nulla ut voluptatum quia consequatur. Laudantium et iure quia et et rerum. Voluptates quod voluptatem commodi dignissimos aut quia aut.')
                ->setModerate("1")
                ->setPrice(11+$i)
                ->setCreatedAt(new \DateTime());
            for ($k = 0; $k <= 2; $k++){
                $foodtype = new Foodtype();
                $typefood = new TypeFood();

                $typefood
                    ->setTitle("Categorie test");

                $foodtype
                    ->setDescription('Reiciendis tempore tempore magnam nulla ut voluptatum quia consequatur.')
                    ->setTitle($foodtypeTitles[$k])
                    ->setTypeFood($typefood)
                    ->addCompomenu($menu);

                $manager->persist($foodtype);
                $manager->flush();
            }

            $manager->persist($menu);
            $manager->flush();
        }

    }

}