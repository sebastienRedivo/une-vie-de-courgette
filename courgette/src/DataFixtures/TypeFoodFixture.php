<?php

namespace App\DataFixtures;

use App\Entity\Menu;
use App\Entity\TypeFood;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TypeFoodFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $foodTypeTab = ["Entrée", "Hors-d'œuvre", "Plat", "Dessert", "Autre"];

        for ($i = 0; $i < count($foodTypeTab); $i++) {
            $typeFood = new TypeFood();
            $typeFood->setTitle($foodTypeTab[$i]);
            $manager->persist($typeFood);
        }
        
        $manager->flush();
    }
}
