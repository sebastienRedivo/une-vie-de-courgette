# humanstxt.org/
# The humans responsible & technology colophon

# TEAM

    jean-baptiste -- développeur
    sebastien -- chef de projet
    judicael -- développeur

# TECHNOLOGY COLOPHON

    CSS3, HTML5, SYMFONY 4
    Apache Server Configs, jQuery, Modernizr, Normalize.css
